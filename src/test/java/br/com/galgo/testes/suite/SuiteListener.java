package br.com.galgo.testes.suite;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.utils.SuiteAplicacaoListener;


/**
 * Created by valdemar.arantes on 05/06/2015.
 */
public class SuiteListener extends SuiteAplicacaoListener {

    @Override
    protected Ambiente getAmbiente() {
        return Ambiente.PRODUCAO;
    }

}
